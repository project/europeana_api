<?php

/**
 * Implementation of hook_schema().
 */
function europeana_api_schema() {
  $schema['cache_europeana_api'] = drupal_get_schema_unprocessed('system', 'cache');
  $schema['cache_europeana_api']['description'] = 'Cache table for the Europeana API module to store full record response in JSON format.';

  return $schema;
}

/**
 * Implementation of hook_install()
 */
function europeana_api_install() {
  drupal_install_schema('europeana_api');
  // install the content type
  $filename = drupal_get_path('module','europeana_api') . "/europeana_api.install.inc";
  $content = file_get_contents($filename); //implode ('', file ($filename));
  // Build form state
  $form_state = array(
     'values' => array(
        'type_name' => '<create>',
        'macro' => $content,
     ),
  );
  drupal_execute('content_copy_import_form', $form_state);
}

/**
 * Implementation of hook_requirements().
 *
 * Requirement checks
 *
 * @param $phase (String)
 * @return (Array)
 *   The requirements of the module.
 */
function europeana_api_requirements($phase) {
  $requirements = array();
  if ($phase == 'runtime') {
    $has_api_key = (variable_get('europeana_api_key', '') != '');

    $requirements['europeana_api_key'] = array(
      'title' => t('Europeana API key'),
      'value' => $has_api_key ? t('Registered') : t('Not registered'),
    );
    if (!$has_api_key) {
      $requirements['europeana_api_key']['severity'] = REQUIREMENT_ERROR;
      $requirements['europeana_api_key']['description'] = t('The Europeana API module will not work properly because the API requires an API key. Please set the API key !here!', array('!here' => l(t('here'), 'admin/settings/europeana')));
    }
  }
  return $requirements;
}

/**
 * Implementation of hook_uninstall.
 *
 * Removes the unnecesary API key
 */
function europeana_api_uninstall() {
  drupal_uninstall_schema('europeana_api');
  variable_del('europeana_api_key');
  // the type_name must be the type_name
  node_type_delete('europeana');
  menu_rebuild();
}
