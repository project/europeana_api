<?php

/**
 * The RSS-based Europeana API's URL
 * @var string
 */
define('EUROPEANA_RSS', 'http://api.europeana.eu/api/opensearch.rss');

/**
 * The JSON-based Europeana API's URL
 * @var string
 */
define('EUROPEANA_JSON', 'http://api.europeana.eu/api/opensearch.json');

/**
 * The Europeana cache table name
 * @var string
 */
define('EUROPEANA_CACHE', 'cache_europeana_api');

/**
 * Implementation of hook_help().
 */
function europeana_api_help($path, $arg) {
  switch ($path) {
    case 'admin/help#block':
      return '<p>'. t('Blocks are boxes of content that may be rendered into certain regions of your web pages, for example, into sidebars. Blocks are usually generated automatically by modules (e.g., Recent Forum Topics), but administrators can also define custom blocks.') .'</p>';

    case 'admin/build/block':
      return t('<p>Blocks are boxes of content that may be rendered into certain regions of your web pages, for example, into sidebars. They are usually generated automatically by modules, but administrators can create blocks manually.</p>
<p>If you want certain blocks to disable themselves temporarily during high server loads, check the "Throttle" box. You can configure the auto-throttle on the <a href="@throttle">throttle configuration page</a> after having enabled the throttle module.</p>
<p>You can configure the behaviour of each block (for example, specifying on which pages and for what users it will appear) by clicking the "configure" link for each block.</p>', array('@throttle' => url('admin/settings/throttle')));
  }
}

/**
 * Implementation of hook_menu().
 */
function europeana_api_menu() {
  $items['admin/settings/europeana'] = array(
    'title' => 'Europeana API key setting',
    'description' => t('Register your Europeana API in order to run search on the site.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('europeana_api_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/settings/europeana/createnode'] = array(
    'title' => 'Europeana API key setting',
    'description' => t('Register your Europeana API in order to run search on the site.'),
    'page callback' => 'europeana_api_createnode',
    'access arguments' => array('administer site configuration'),
    'type' => MENU_CALLBACK,
  
  );
  
  return $items;
}

/**
 * Implementation of hook_theme().
 */
function europeana_api_theme() {
  $path = drupal_get_path('module', 'europeana_api') . '/theme';

  return array(
    'europeana_api_result' => array(
      'arguments' => array('result' => NULL, 'type' => NULL),
      'file' => 'europeana_api.pages.inc',
      'template' => 'search-result-europeana',
    ),
    'europeana_api_results' => array(
      'arguments' => array('results' => NULL, 'type' => NULL),
      'file' => 'europeana_api.pages.inc',
      'template' => 'search-results-europeana',
    ),
    'europeana_individual_metadata' => array(
      'arguments' => array('label' => NULL, 'value' => NULL),
    ),
    'europeana_snippet' => array(
      'arguments' => array('image' => NULL, 'metadata' => NULL),
    ),
    'content_field' => array(
      'template' => 'content-field-europeana',
      'arguments' => array('element' => NULL),
      'path' => $path,
    ),
    'content_field_europeana' => array(
      'template' => 'content-field-europeana',
      'arguments' => array('element' => NULL),
      'path' => $path,
    ),
    'node_europeana' => array(
      'arguments' => array('node' => NULL, 'teaser' => FALSE, 'page' => FALSE),
      'template' => 'node-europeana',
      'path' => $path,
    ),
  );
}

/**
 * Implementation of hook__theme_registry_alter().
 * 
 * It adds the module's theme path to node's theme paths in the theme registry,
 * so Drupal will look for this directory as well, when looking for candidates.
 */
function europeana_api_theme_registry_alter(&$theme_registry) {
  $theme_registry['node']['theme paths'][] = drupal_get_path('module', 'europeana_api') . '/theme';
}

/*
function theme_content_field_europeana($element) {
  
}
*/

/**
 * Settings form for the API key
 */
function europeana_api_settings() {

  $form['europeana_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Europeana API key'),
    '#description' => t('Please input your Europeana API key. If you don\'t have, you can require !here.', 
        array('!here' => l(t('here'), 'http://pro.europeana.eu/web/guest/registration'))),
    '#default_value' => variable_get('europeana_api_key', ''),
    '#size' => 20
  );

  return system_settings_form($form);
}

function europeana_api_search($op = 'search', $keys = null) {
  // $output = drupal_render(drupal_get_form('europeana_api_search_form'));
  switch ($op) {
    case 'search':
      $output = europeana_api_do_search($keys); break;
    case 'name':
      $output = t('Europeana'); break;
    case 'admin':
      $output = europeana_api_settings(); break;
  }
  return $output;
}

function europeana_api_do_search($term) {
  global $pager_page_array, $pager_total, $pager_total_items;

  $results = array();
  $response = europeana_api_retrieve_results($term);
  if (!$response) {
    return $results;
  }

  if ($response->code != 200) {
    drupal_set_message($response->code);
    return array();
  }
  $europeana_result = json_decode($response->data);
  $pager_total_items[0] = $europeana_result->totalResults;
  $pager_total[0] = ceil($europeana_result->totalResults / $europeana_result->itemsPerPage);
  
//'user': The author of the item.
//'extra': An array of optional extra information items.
  $do_retrieve_full_record = false;
  drupal_http_request($url);
  foreach ($europeana_result->items as $record) {
    $full_record = $do_retrieve_full_record ? europeana_api_retrieve_full_record($record->link) : $record;
    $id = str_replace(array('http://www.europeana.eu/portal/record/', '.html'), '', $record->guid);
    $results[] = array(
      'link' => $record->guid,
      'title' => $record->title,
      'type' => 'europeana', //$record->{'europeana:type'},
      // 'date' => $record->{'europeana:year'},
      'extra' => array('create_node' => l(
        t('Create node from this document'), 
        'admin/settings/europeana/createnode/' . $id
        // array('query' => array('guid' => $record->guid))
        )),
      'snippet' => europeana_api_create_snippet($full_record),
    );
  }
  if (count($results) > 0) {
    drupal_add_css(drupal_get_path('module', 'europeana_api') . '/europeana_api.css');
  }
  return $results;
}

/**
 * Retrieves the full record, and using cache to store/retrieve result from the server.
 *
 * @param (string) $url
 *   The full record JSON representation's URL
 */
function europeana_api_retrieve_full_record($url) {
  $cid = 'europeana:' . md5($url);

  $cached = cache_get($cid, EUROPEANA_CACHE);
  if ($cached) {
    $full_record = $cached->data;
  }
  else {
    $full_record_response = drupal_http_request($url);
    $full_record = json_decode($full_record_response->data);
    cache_set($cid, $full_record, EUROPEANA_CACHE);
  }

  return $full_record;
}

function europeana_api_create_snippet($record, $is_full = false) {
  static $metadata;
  if (!isset($metadata_fields)) {
    $metadata_fields = array(
      t('Creator') => 'dc:creator',
      t('Contributor') => 'dc:contributor',
      t('Date') => array('dc:date', 'europeana:year', 'enrichment:period_label'),
      t('Geographic coverage') => 'dcterms:spatial',
      t('Type') => array('dc:type', 'europeana:type'),
      t('Description') => 'dc:description',
      t('Subject') => 'dc:subject',
      t('Provider') => array('europeana:dataProvider', 'europeana:provider'),
    );
  }

  $metadata = array();
  foreach ($metadata_fields as $label => $field_name) {
    $value = NULL;
    if (is_array($field_name)) {
      foreach ($field_name as $name) {
        if (isset($record->$name)) {
          $value = $record->$name;
          if ($label == t('Type')) {
            if ($name == 'europeana:type') {
              $value = europeana_translate_type($value);
            }
          }
          break;
        }
      }
    }
    else {
      $value = $record->$field_name;
    } 
    europeana_api_set($metadata, $label, $value);
  }
  $metadata = join('<br/>', $metadata);
  $image_url = 'http://europeanastatic.eu/api/image?type=IMAGE&size=BRIEF_DOC';
  if (!empty($record->enclosure)) {
    $image_url .= '&uri=' . urlencode($record->enclosure);
  }
  $image = l(
    theme('image', $image_url, $record->description, $record->title, array(), FALSE), 
    $record->guid, 
    array('html' => TRUE)
  );
  return theme('europeana_snippet', $image, $metadata);
}

/**
 * Makes translates the Europeana types
 * 
 * @param (string) $type
 *   The Europeana type (IMAGE, TEXT, VIDEO, or SOUND)

 * @return (string)
 *   The translated type
 */
function europeana_translate_type($type) {
  static $translations;
  if (!isset($translations)) {
    $translations = array(
      'IMAGE' => t('Image'), 
      'TEXT' => t('Text'),
      'VIDEO' => t('Video'),
      'SOUND' => t('Sound'),
    );
  }
  
  if (isset($translations[$type])) {
    return $translations[$type];
  }
  return $type;
}


function europeana_api_set(&$output, $label, $value) {
  if (is_null($value) || empty($value)) {
    return;
  }

  if (is_array($value)) {
    $value = join(' &ndash; ', $value);
  }
  
  $output[] = theme('europeana_individual_metadata', $label, $value);
}

function theme_europeana_individual_metadata($label, $value) {
  $output = theme('placeholder', $label) . ': ' . $value;
  return $output;
}

function theme_europeana_snippet($image, $metadata) {
  $output = '<span class="europeana-snippet">'
          . '<span class="europeana-thumbnail">' . $image . '</span>'
          . '<span class="europeana-metadata">' . $metadata . '</span>'
          . '</span>';
  return $output;
}

function europeana_api_perm() {
  return array('create europeana node', 'edit own europeana nodes');
}

function europeana_api_access($op, $node, $account) {

  if ($op == 'create') {
    // Only users with permission to do so may create this node type.
    return user_access('create europeana node', $account);
  }

  // Users who create a node may edit or delete it later, assuming they have the
  // necessary permissions.
  if ($op == 'update' || $op == 'delete') {
    if (user_access('edit own europeana node', $account) && ($account->uid == $node->uid)) {
      return TRUE;
    }
  }
}

function europeana_api_createnode() {
  $schema2drupal = array(
    'europeana:uri' => 'field_europeana_uri',
    'europeana:country' => 'field_europeana_country',
    'europeana:provider' => 'field_europeana_provider',
    'europeana:collectionName' => 'field_europeana_collection_name',
    'europeana:isShownAt' => 'field_europeana_is_shown_at',
    'europeana:isShownBy' => 'field_europeana_is_shown_by',
    'europeana:object' => 'field_europeana_object',
    'europeana:language' => 'field_europeana_language',
    'europeana:type' => 'field_europeana_type',
    'europeana:year' => 'field_europeana_year',
    'europeana:rights' => 'field_europeana_rights',
    'europeana:dataProvider' => 'field_europeana_data_provider',
    'europeana:completeness' => 'field_europeana_completeness',
    'dcterms:alternative' => 'field_dcterms_alternative',
    'dcterms:issued' => 'field_dcterms_issued',
    'dcterms:spatial' => 'field_dcterms_spatial',
    'dc:creator' => 'field_dc_creator',
    'dc:contributor' => 'field_dc_contributor',
    'dc:coverage' => 'field_dc_coverage',
    'dc:description' => 'field_dc_description',
    'dc:format' => 'field_dc_format',
    'dc:identifier' => 'field_dc_identifier',
    'dc:language' => 'field_dc_language',
    'dc:publisher' => 'field_dc_publisher',
    'dc:relation' => 'field_dc_relation', //
    'dc:source' => 'field_dc_source',
    'dc:subject' => 'field_dc_subject',
    'dc:title' => 'field_dc_title',
    'dc:type' => 'field_dc_type',
    'enrichment:place_term' => 'field_enr_place_term',
    'enrichment:place_label' => 'field_enr_place_label',
    'enrichment:place_latitude' => 'field_enr_place_latitude',
    'enrichment:place_longitude' => 'field_enr_place_longitude',
    'enrichment:place_broader_term' => 'field_enr_place_broader_term',
    'enrichment:place_broader_label' => 'field_enr_place_broader_label',
    'enrichment:concept_term' => 'field_enr_concept_term',
    'enrichment:concept_label' => 'field_enr_concept_label',
    'enrichment:concept_broader_term' => 'field_enr_concept_broader_term',
    'enrichment:concept_broader_label' => 'field_enr_concept_broader_label',
    'enrichment:period_term' => 'field_enr_period_term',
    'enrichment:period_label' => 'field_enr_period_label',
    'enrichment:period_begin' => 'field_enr_period_begin',
    'enrichment:period_end' => 'field_enr_period_end',
    'enrichment:period_broader_term' => 'field_enr_period_broader_term',
    'enrichment:period_broader_label' => 'field_enr_period_broader_label',
  );
  $full_record = FALSE;
  $coll_id = arg(4);
  $rec_id = arg(5);
  if (!empty($coll_id) && !empty($rec_id)) {
    $api_key = variable_get('europeana_api_key', '');
    if ($api_key != '') {
      $full_url = 'http://www.europeana.eu/portal/record/' . $coll_id . '/' . $rec_id . '.json?wskey=' . $api_key;
      $full_record = europeana_api_retrieve_full_record($full_url);
    }
  }

  $node = new stdClass();
  $node->type = 'europeana';
  if (is_array($full_record->{'dc:title'})) {
    $node->title = join(' - ', $full_record->{'dc:title'});
  }
  else {
    $node->title = $full_record->{'dc:title'};
  }
  // $node->body = $full_record->{'europeana:type'} . ' -- ' . $full_record->{'europeana:provider'};
  // check
  foreach ($full_record as $schema_name => $value) {
    if (!isset($schema2drupal[$schema_name])) {
      dpm(t('Unregistered field name: @name', array('@name' => $schema_name)));
    }
  }
  
  foreach ($schema2drupal as $schema_name => $field_name) {
    if (is_array($full_record->$schema_name)) {
      foreach ($full_record->$schema_name as $value) {
        $node->{$field_name}[]['value'] = $value;
      }
    }
    else {
      $node->{$field_name}[]['value'] = $full_record->$schema_name;
    }
  }
  node_save($node);
  
  drupal_set_message(t('You successfully created a new Europeana node!'));
  drupal_goto('node/' . $node->nid);
}

function europeana_api_view($node, $teaser = FALSE, $page = FALSE) {
  // If $teaser is FALSE, the entire node is being displayed.
  if (!$teaser) {
    // Use Drupal's default node view.
    $node = node_prepare($node, $teaser);
    $node->content['europeana'] = array(
      '#value' => theme('europeana_node', $node),
      '#weight' => 2
    );
  }
  
  if ($teaser) {
    // Use Drupal's default node view.
    $node = node_prepare($node, $teaser);
   }
   return $node;
}

function theme_europeana_node($node) {
  
}

/**
 * Retrieve results from Europeana API
 *
 * @param (string) $term
 *   The query term
 *
 * @return (object)
 *   The Drupal result object, or FALSE if the API key is not set.
 */
function europeana_api_retrieve_results($term) {
  $api_key = variable_get('europeana_api_key', '');
  if ($api_key == '') {
    drupal_set_message(t('The Europeana API key is not set correctly in this site. Please report the problem to site administrator.'));
    return FALSE;
  }

  $params = array(
    'searchTerms' => check_plain($term),
    'startPage' => isset($_GET['page']) ? (int)$_GET['page']+1 : 1,
    'wskey' => $api_key,
  );

  $url = EUROPEANA_JSON . '?' . http_build_query($params, '', '&');
  $response = drupal_http_request($url);

  return $response;
}

function europeana_api_preprocess_content_field_europeana(&$variables, $hook) {
  if ($variables['element']['#node']->type == 'europeana') {
    template_preprocess_content_field($variables);
  }
}

function europeana_api_preprocess_node(&$variables) {
  if ($variables['node']->type == 'europeana') {
    $node = $variables['node'];
    $variables['extra'] = array();
    $fields = array_keys($node->content);
    // array_search
    $field_index = array_flip(array_keys($node->content));
    unset($fields[$field_index['body']]);
    unset($fields[$field_index['#content_extra_fields']]);
    unset($fields[$field_index['#pre_render']]);
    unset($fields[$field_index['#title']]);
    unset($fields[$field_index['#description']]);
    unset($fields[$field_index['#children']]);
    unset($fields[$field_index['#printed']]);

    if (!empty($node->field_europeana_uri)) {
      $values = array();
      foreach ($node->field_europeana_uri as $i => $term) {
        if (_europeana_api_is_url($term['value'])) {
          $term['value'] = l($term['value'], $term['value']);
        }
        $values[] = $term['value'];
      }
      $variables['extra']['field_europeana_uri'] = array('label' => t('Europeana URI'), 'value' => join(' &ndash; ', $values));
      $fields[$field_index['field_europeana_uri']] = array('field_europeana_uri', 'extra');
    }

    if (!empty($node->field_europeana_rights)) {
      $values = array();
      foreach ($node->field_europeana_rights as $i => $term) {
        if (_europeana_api_is_url($term['value'])) {
          $term['value'] = l($term['value'], $term['value']);
        }
        $values[] = $term['value'];
      }
      $variables['extra']['field_europeana_rights'] = array('label' => t('Europeana Rights'), 'value' => join(' &ndash; ', $values));
      $fields[$field_index['field_europeana_rights']] = array('field_europeana_rights', 'extra');
    }

    if (!empty($node->field_dc_identifier)) {
      $values = array();
      foreach ($node->field_dc_identifier as $i => $term) {
        if (_europeana_api_is_url($term['value'])) {
          $term['value'] = l($term['value'], $term['value']);
        }
        $values[] = $term['value'];
      }
      $variables['extra']['field_dc_identifier'] = array('label' => t('dc:identifier'), 'value' => join(' &ndash; ', $values));
      $fields[$field_index['field_dc_identifier']] = array('field_dc_identifier', 'extra');
    }
    
    if (!empty($node->field_europeana_object)) {
      $base_obj_url = 'http://europeanastatic.eu/api/image?type=IMAGE&size=FULL_DOC&uri=';
      $values = array();
      foreach ($node->field_europeana_object as $i => $term) {
        if (empty($term['value'])) {
          continue;
        }
        if (_europeana_api_is_url($term['value'])) {
          $term['value'] = theme('image', $base_obj_url . urlencode($term['value']), $node->title, $node->title, NULL, FALSE);
        }
        $values[] = $term['value'];
      }
      if (!empty($values)) {
        $variables['extra']['field_europeana_object'] = array('label' => t('Europeana object'), 'value' => join('<br/>', $values));
        $fields[$field_index['field_europeana_object']] = array('field_europeana_object', 'extra');
      }
    }

    if (!empty($node->field_enr_place_term)) {
      if (count($node->field_enr_place_term) == 1 
          && _europeana_api_is_url($node->field_enr_place_term[0]['value'])) {
        $labels = array();
        foreach ($node->field_enr_place_label as $label) {
          $labels[] = $label['value'];
        }
        $url = _europeana_api_correct_geonames($node->field_enr_place_term[0]['value']);
        $value = l(join(' &ndash; ', $labels), $url, array('html' => TRUE));
        
        if (isset($node->field_enr_place_latitude) || isset($node->field_enr_place_longitude)) {
          $value .= ' (';
          if (isset($node->field_enr_place_latitude)) {
            $value .= $node->field_enr_place_latitude[0]['value'];
          }
          if (isset($node->field_enr_place_latitude) && isset($node->field_enr_place_longitude)) {
            $value .= ', ';
          }
          if (isset($node->field_enr_place_longitude)) {
            $value .= $node->field_enr_place_longitude[0]['value'];
          }
          $value .= ')';
        }
        $variables['extra']['place'] = array('label' => t('place'), 'value' => $value);
        $fields[$field_index['field_enr_place_label']] = array('place', 'extra');
        unset($fields[$field_index['field_enr_place_term']]);
        unset($fields[$field_index['field_enr_place_latitude']]);
        unset($fields[$field_index['field_enr_place_longitude']]);
      }
      else {
        $values = array();
        foreach ($node->field_enr_place_term as $i => $term) {
          if (_europeana_api_is_url($term['value'])) {
            $values[] = l($term['value'], _europeana_api_correct_geonames($term['value']));
          }
        }
        $variables['extra']['place_term'] = array('label' => t('place term'), 'value' => join(' &mdash; ', $values));
        $fields[$field_index['field_enr_place_term']] = array('place_term', 'extra');
      }
    }

    if (!empty($node->field_enr_place_broader_term)) {
      if (count($node->field_enr_place_broader_term) == 1
            && _europeana_api_is_url($node->field_enr_place_broader_term[0]['value'])) {
        $labels = array();
        foreach ($node->field_enr_place_broader_label as $label) {
          $labels[] = $label['value'];
        }
        $url = _europeana_api_correct_geonames($node->field_enr_place_broader_term[0]['value']);
        $variables['extra']['place_broader'] = array('label' => t('place &ndash; broader'), 'value' => l(join(' &ndash; ', $labels), $url, array('html' => TRUE)));
        $fields[$field_index['field_enr_place_broader_label']] = array('place_broader', 'extra');
        unset($fields[$field_index['field_enr_place_broader_term']]);
      }
      else {
        foreach ($node->field_enr_place_broader_term as $i => $term) {
          if (_europeana_api_is_url($term['value'])) {
            $node->field_enr_place_broader_term[$i]['value'] = l($term['value'], $term['value']);
          }
        }
      }
    }
    
    if (!empty($node->field_enr_concept_term) 
        && count($node->field_enr_concept_term) == 1 
        && _europeana_api_is_url($node->field_enr_concept_term[0]['value'])) {
      $labels = array();
      foreach ($node->field_enr_concept_label as $label) {
        $labels[] = $label['value'];
      }
      $url = _europeana_api_correct_geonames($node->field_enr_concept_term[0]['value']);
      $variables['extra']['concept'] = array('label' => t('concept'), 'value' => l(join(' &ndash; ', $labels), $url, array('html' => TRUE)));
      $fields[$field_index['field_enr_concept_label']] = array('concept', 'extra');
      unset($fields[$field_index['field_enr_concept_term']]);
    }
    
    if (!empty($node->field_enr_concept_boarder_term) 
        && count($node->field_enr_concept_boarder_term) == 1 
        && preg_match('/^http:[^ ]+$/', $node->field_enr_concept_boarder_term[0]['value'])) {
      $labels = array();
      foreach ($node->field_enr_concept_boarder_label as $label) {
        $labels[] = $label['value'];
      }
      $url = _europeana_api_correct_geonames($node->field_enr_concept_boarder_term[0]['value']);
      $variables['extra']['concept_broader'] = array('label' => t('concept &ndash; broader'), 'value' => l(join(' &ndash; ', $labels), $url, array('html' => TRUE)));
      $fields[$field_index['field_enr_concept_boarder_label']] = array('concept_broader', 'extra');
      unset($fields[$field_index['field_enr_concept_boarder_term']]);
    }

    if (!empty($node->field_enr_period_term) 
        && count($node->field_enr_period_term) == 1 
        && preg_match('/^http:[^ ]+$/', $node->field_enr_period_term[0]['value'])) {
      $labels = array();
      foreach ($node->field_enr_period_label as $label) {
        $labels[] = $label['value'];
      }
      $url = _europeana_api_correct_geonames($node->field_enr_period_term[0]['value']);
      $variables['extra']['field_enr_period_label'] = array(
        'label' => t('period'), 
        'value' => l(join(' &ndash; ', $labels), $url, array('html' => TRUE))
      );
      $fields[$field_index['field_enr_period_label']] = array('field_enr_period_label', 'extra');
      unset($fields[$field_index['field_enr_period_term']]);
    }

    if (!empty($node->field_enr_period_broader_term)) {
      if (count($node->field_enr_period_broader_term) == 1 
          && preg_match('/^http:[^ ]+$/', $node->field_enr_period_broader_term[0]['value'])) {
        $labels = array();
        foreach ($node->field_enr_period_broader_label as $label) {
          $labels[] = $label['value'];
        }
        $url = $node->field_enr_period_broader_term[0]['value'];
        $variables['extra']['field_enr_period_broader_label'] = array(
          'label' => t('period &ndash; broader'), 
          'value' => l(join(' &ndash; ', $labels), $url, array('html' => TRUE))
        );
        $fields[$field_index['field_enr_period_broader_label']] = array('field_enr_period_broader_label', 'extra');
        unset($fields[$field_index['field_enr_period_broader_term']]);
      }
      else {
        $values = array();
        foreach ($node->field_enr_period_broader_term as $i => $term) {
          if (_europeana_api_is_url($term['value'])) {
            $values[] = l($term['value'], $term['value']);
          }
        }
        $variables['extra']['field_enr_period_broader_term'] = array('label' => t('period &ndash; broader'), 'value' => join(' &mdash; ', $values));
        $fields[$field_index['field_enr_period_broader_term']] = array('field_enr_period_broader_term', 'extra');
      }
    }
    
    $variables['field_list'] = $fields;
  }
}

function _europeana_api_is_url($text) {
  return preg_match('/^http:[^ ]+$/', $text);
}

function _europeana_api_correct_geonames($text) {
  return str_replace('sws.geonames.org', 'www.geonames.org', $text);
}