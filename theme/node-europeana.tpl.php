<?php
?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

<?php print $picture ?>

<?php if ($page == 0): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>

  <div class="content clear-block">
    <?php foreach ($field_list as $field) : ?>
      <?php if (is_string($field)) : ?>
        <?php print theme('content_field_europeana', $node->content[$field]['field']); ?>
      <?php else : ?>
        <div class="field field-type-text">
          <div class="field-label"><?php print $extra[$field[0]]['label']; ?>:&nbsp;</div>
          <div class="field-items"><?php print $extra[$field[0]]['value']; ?></div>
        </div>
      <?php endif; ?>
    <?php endforeach; ?>
  </div>

  <div class="clear-block">
    <div class="meta">
    <?php if ($taxonomy): ?>
      <div class="terms"><?php print $terms ?></div>
    <?php endif;?>
    </div>

    <?php if ($links): ?>
      <div class="links"><?php print $links; ?></div>
    <?php endif; ?>
  </div>

</div>
